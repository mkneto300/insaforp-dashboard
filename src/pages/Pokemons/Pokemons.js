import PokemonList from "../../components/PokemonList";

export default function Pokemons() {
    return (
        <>
            <header>Pokemons</header>
            <PokemonList />
        </>
    );
}