import {useState} from 'react';
import SearchProduct from "../../../components/SearchProduct";
import ShowProduct from '../../../components/ShowProduct';

export default function FindProudct(){    
const [product, setProduct] = useState(null);

    return <>
            <header className="text-center">
                <h3>Find Product</h3>
                <small>Search a product by its ID</small>
            </header>
            <SearchProduct setProduct={setProduct} />
            {product && <ShowProduct product={product} />}
        </>
}