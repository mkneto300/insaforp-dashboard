import ProductList from "../../components/ProductList"

export default function Products() {
    return(
        <>
            <header className="text-center">
                <h3>Products</h3>
            </header>
            <ProductList />
        </>
    )
}