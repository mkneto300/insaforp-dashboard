import { Row } from 'react-bootstrap';
import plansData from '../../data/plans.json';
import CardPlan from './CardPlan';

export default function Plans() {    
    return <Row className='row justify-content-center h-100'>{plansData.map((plan, index) => (<CardPlan key={index} plan={plan} />))}</Row>;
;

}