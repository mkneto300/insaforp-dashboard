import {Col, Card, Button} from 'react-bootstrap';

const planColorSelector = {
    'basic': ['text-primary', 'bg-primary bg-gradient'],
    'standard': ['text-dark', 'bg-dark bg-gradient'],
    'premium': ['text-danger', 'bg-danger bg-gradient'],
};

const ligtherBgColor = {
    backgroundColor: 'rgb(180 187 193 / 3%)'
}

export default function CardPlan({plan}){
    const planColors = planColorSelector[plan.name.toLowerCase()];
    return(
        <Col xs={12} md={3} className='h-50'>
            {/* <Card style={{ width: '18rem' }}>   */}      
            <Card className="h-100">
                <Card.Header className="border-bottom-0" style={ligtherBgColor}>
                    <div className="text-center">
                        <Card.Title className="fs-1" style={{letterSpacing: '0.2rem'}}>
                            <span className={planColors[0]}>{plan.name}</span>
                        </Card.Title>                        
                    </div>
                </Card.Header>
                <Card.Body>
                    {plan.features.map((feature,index) => (
                        <div key={index} className="mb-2">
                            <div className="d-inline-block mr-2">
                                { feature.available ? <i className="bi bi-check-lg fw-bolder text-success"></i> : <i className="bi bi-x-lg fw-bolder text-danger"></i> }
                            </div>                            
                            <div className="d-inline-block mr-2">{feature.name}</div>
                        </div>
                    ))}
                </Card.Body>
                <Card.Footer className='text-center border-top-0' style={ligtherBgColor}>
                    <div className="text-center mb-3">
                        <Card.Title> 
                            <span className='fs-3'>${plan.price} </span>                           
                            <br />
                            <small><i className="text-muted"> {plan.subscription }</i></small>                            
                        </Card.Title>                        
                    </div>
                    <Button className={planColors[1]} >Select</Button>
                </Card.Footer>
                </Card>
        </Col>      
    );
}