import logo from './logo.svg';
import './App.css';
import AppUI from './components/AppUI';

function App() {
  return (
    <AppUI />
  );
}

export default App;
