import Dashboard from "./pages/Dashboard";
import Contacts from "./pages/Contacts";
import AddContact from "./pages/Contacts/AddContact";
import Pokemons from "./pages/Pokemons";
import Plans from "./pages/Plans";
import Products from "./pages/Products";
import Product from "./pages/Products/Product";
import FindProduct from "./pages/Products/FindProduct";
import {Routes, Route} from "react-router-dom";

export default function AppRoutes() {
    return(        
        <Routes>
          <Route path="/contacts/add" element={<AddContact />} />
          <Route path="/contacts" element={<Contacts />} />
          <Route path="/plans" element={<Plans />} />
          <Route path="/pokemons" element={<Pokemons />} />
          <Route path="/products/find" element={<FindProduct />} />
          <Route path="/products/:id" element={<Product />} />
          <Route path="/products" element={<Products />} />
          <Route path="/" element={<Dashboard />} /> 
        </Routes>
    );
}