import {useState, useEffect} from 'react';

const requestOptions = {
  method: 'GET',
  redirect: 'follow'
};

const fetchPokemonData = async (setPokemons) => {
    const pokemons = [];
    try {
        const firstResponse = await fetch("https://pokeapi.co/api/v2/pokemon?limit=10&offseet=10", requestOptions);
        if (firstResponse.ok) {
            const firstData = await firstResponse.json();
            const pokemonArray = firstData.results;
            for(const pokemon of pokemonArray){
                const secondResponse = await fetch(pokemon.url);
                if (secondResponse.ok) {
                    const secondData = await secondResponse.json();
                    pokemon.data = secondData;                    
                    pokemons.push(pokemon);
                } else {
                    console.error("Error fetching second call");
                }                
            }
        } else {
            console.error("Error fetching first call");
        }
    } catch (error) {
        console.log(error);
    }    
    setPokemons(pokemons);
}

export default function PokemonList(){
    const [pokemons, setPokemons] = useState([]);

    useEffect(() => {
        /* fetch("https://pokeapi.co/api/v2/pokemon?limit=10&offseet=10", requestOptions)
        .then(response => response.json())
        .then(result => setPokemons(result.results))
        .catch(error => console.log('error', error)); */
        fetchPokemonData(setPokemons);        
    },[]);    

    return(<ul>
        {
            pokemons?.map(pokemon => (
                <li key={pokemon.url} style={{listStyleType: "none"}}>
                    <b>{pokemon.name}</b>
                    <img src={pokemon.data.sprites.front_default} alt={pokemon.name + " front"} />
                </li>
            ))
        }
    </ul>);
}