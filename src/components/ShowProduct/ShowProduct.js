import { renderProduct } from "../ProductDetails/ProductDetails";
import {Breadcrumb } from 'react-bootstrap/';
import { Link } from 'react-router-dom';

export default function ShowProduct({product}){
    return(
        <>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link to="/">Home</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>                    
                    <Link to="/products">Products</Link>
                </Breadcrumb.Item>                                   
            </Breadcrumb>            
            {renderProduct(product)}
        </>
    )
}