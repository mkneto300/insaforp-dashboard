import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import './SideBar.css';
import {Link} from 'react-router-dom';

export default function SideBar() {
  return(
    <Nav defaultActiveKey="/home" className="flex-sm-column nav-height" justify={false} >
      <Nav.Link>
        <Link className='text-white text-decoration-none' to="/">Home</Link>
      </Nav.Link>
      <NavDropdown title="Contacts" id="contacts-dropdown" className='text-white text-decoration-none' >
        <NavDropdown.Item>
            <Link to="/contacts">Contacts</Link>
        </NavDropdown.Item>
        <NavDropdown.Item>
            <Link to="/contacts/add">Add Contact</Link>
        </NavDropdown.Item>
      </NavDropdown>
      <Nav.Link>
        <Link className='text-white text-decoration-none' to="/plans">Plans</Link>
      </Nav.Link>
      <Nav.Link>
        <Link className='text-white text-decoration-none' to="/pokemons">Pokemons</Link>
      </Nav.Link>
      <NavDropdown title="Products" id="products-dropdown" className='text-white text-decoration-none' >
        <NavDropdown.Item>
            <Link to="/products">Products</Link>
        </NavDropdown.Item>
        <NavDropdown.Item>
            <Link to="/products/find">Find product</Link>
        </NavDropdown.Item>
      </NavDropdown>
    </Nav>
  );
}