import Container from 'react-bootstrap/Container';

export default function MainContent({children}) {
    return(
        <Container fluid className="h-100 pt-5">
            {children}
        </Container>
    );
}