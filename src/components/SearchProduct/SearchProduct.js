import {useState, useEffect} from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';

export default function SearchProduct({setProduct}) {
    const [productId, setProductId] = useState(null);

    const clickHandler = (e) => {
        e.preventDefault();
        if ( productId === "" || productId.length <= 0) {
            alert("Please enter a value for product id");
        } else {
            fetch(`https://fakestoreapi.com/products/${productId}`)
            .then(res=>res.json())
            .then(json=>setProduct(json))
            .catch(error => setProduct("hubo un error"))
        }
    }

    const handleChange = (e) => {        
        setProductId(e.target.value);
    }

    return(
        <>
        <InputGroup className="mb-3">
            <Form.Control
            value={productId}
            onChange={handleChange}
            placeholder="Product id"
            aria-label="Product id"
            aria-describedby="search product input"
            />
            <Button variant="outline-secondary" id="search-product-button" onClick={clickHandler}>
                Search
            </Button>
      </InputGroup>
        </>
    )
}