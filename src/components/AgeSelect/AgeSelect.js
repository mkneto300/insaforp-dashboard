import Form from 'react-bootstrap/Form';

export default function AgeSelect() {
  const options = [];
  let age = 18;

  while (age <= 100) {
    options.push(<option key={age} value={age}>{age}</option>);
    age++;
  }

  return (
    <Form.Select aria-label="Select contact age">
      {options}
    </Form.Select>
  );
}