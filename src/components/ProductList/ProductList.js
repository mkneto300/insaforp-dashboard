import { useState, useEffect } from "react";
import Table from 'react-bootstrap/Table';
import ProductItem from "./ProductItem";

export default function ProductList(){
    const [products, setProducts] = useState([]);

    useEffect(() =>{
        fetch('https://fakestoreapi.com/products')
            .then(res=>res.json())
            .then(json=>setProducts(json))
            .catch((error) => console.error(error));
    },[]);

    
    return(<Table className="table table-striped">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Price</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
            </tr>
        </thead>
        <tbody>
            {products.map(product => <ProductItem product={product}  />)}
        </tbody>
    </Table>);
}