import { useNavigate } from "react-router-dom";
import './ProductItem.css';

export default function ProductItem({product}){

    const navigate = useNavigate();

    const handleClick = (e) => {
        navigate(`/products/${product.id}`);
    };

    return(
        <tr onClick={handleClick} className="product-item">
          <th scope="row">{product.id}</th >
          <td>{product.title}</td>
          <td>${product.price}</td>
          <td>{product.description}</td>
          <td><img src={product.image} alt={product.title} width={150} height={150} /></td>
        </tr>
    );
}