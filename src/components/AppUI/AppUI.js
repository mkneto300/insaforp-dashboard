import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import SideBar from "../SideBar";
import MainNav from '../MainNav';
import MainContent from '../MainContent';
import {BrowserRouter as Router} from 'react-router-dom';
import AppRoutes from '../../AppRoutes';

export default function AppUI() {
    return(    
        <Router>
            <Container fluid>
                <Row>
                    <Col>
                        <MainNav />
                    </Col>
                </Row>
                <Row>
                    <Col xs={3} md={1} className='bg-secondary bg-gradient'>
                        <SideBar />
                    </Col>
                    <Col>                        
                        <MainContent>
                            <AppRoutes />
                        </MainContent>
                    </Col>
                </Row>            
            </Container>        
        </Router>
    );
}