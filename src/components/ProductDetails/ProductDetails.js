import {useState, useEffect} from 'react';
import Card from 'react-bootstrap/Card';
import {Breadcrumb, Badge} from 'react-bootstrap/';
import { Link } from 'react-router-dom';
import productNotFound from '../../images/product-not-found.png';

export default function ProductDetails() {

    const [product, setProduct] = useState(null);

    useEffect(() => {
        fetch(`https://fakestoreapi.com${window.location.pathname}`)
            .then(res=>res.json())
            .then(json=>setProduct(json))
            .catch(error=>setProduct("hubo un error"));
    },[]);
    
    
    return(
        <>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <Link to="/">Home</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>                    
                    <Link to="/products">Products</Link>
                </Breadcrumb.Item>
                { product !== "hubo un error" ? <Breadcrumb.Item active> {product?.title.slice(0, 15).concat('...')} </Breadcrumb.Item> : ""  }                    
            </Breadcrumb>                         
            {renderProduct(product)}            
        </>

    );
}

const renderProduct = (product) => {
    if(product === "hubo un error") {
        return (
            <div className="text-center">            
                    <img src={productNotFound} alt="Not found product" />                
                <h3>No se encontro el producto</h3>
                <h5 className="text-danger">Verifique que la informacion enviada es correcta.</h5>
            </div>
        )
    }
    else if(product) {
        return(
            <div className="d-flex justify-content-center">
                <Card style={{ width: '30rem' }}>
                <Card.Img variant="top" src={product.image} />
                <Card.Body className='text-center'>
                    <Card.Title>{product.title}</Card.Title>
                    <Card.Text>
                        <span> <b>${product.price}</b> </span> <br />
                        <Badge bg="primary">{product.category}</Badge> <br />                    
                    </Card.Text>
                    <Card.Text style={{textAlign: "justify"}}>
                        <span>{product.description}</span>
                    </Card.Text>
                </Card.Body>
                </Card>
            </div>
        )
    } else {
        return <h3 className="text-center">Loading...</h3>
    }

}

export {
    renderProduct
}